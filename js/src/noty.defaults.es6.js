/**
 * Setup noty library.
 */
Noty.overrideDefaults({
  type: 'info',
  layout   : 'bottomRight',
  theme    : 'metroui',
  closeWith: ['click', 'button'],
  timeout: 10000
});
