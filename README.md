# Noty Messages

## Introduction
Noty Messages is a library that brings in cuztomization available in the noty
jQuery plugin (http://needim.github.com/noty/) into drupal, and allows the the
library's rendering of messages overtake Drupal's default message theming style.
The goal of the module is be extremely flexible and allow overriding of only
certain Drupal message types if that's what the user wishes.

If you would like to test out the library, visit http://needim.github.com/noty/

## Installation
 1. First you need set some custom entries on your composer.json file in order to install the Noty library dependency. 
    Typically you will use drupal-composer/drupal-project base to create your project, if that your case you can skip 
    to the next step. Most Drupal scaffolding projects are shipped with this settings by default.
    
    That been said, make sure to add this lines to your composer.json and replace *web* with your project docroot
    (this is where the Drupal core is located).
    
       ```JSON
       "repositories": [
         {
           "type": "composer",
           "url": "https://asset-packagist.org"
         }
       ]
       "installer-types": ["npm-asset", "bower-asset"],
       "installer-paths": {
         "web/libraries/{$name}": [
           "type:drupal-library",
           "vendor:npm-asset",
           "vendor:bower-asset"	
         ],
       }
       ```
    The reason behind this is that dependencies cannot register repositories for the root composer.json, [check this](https://getcomposer.org/doc/faqs/why-can%27t-composer-load-repositories-recursively.md) for more information.
    
 1. Install Noty library `composer require npm-asset/noty:"^3.1"`
 1. Just as any other module add it using Composer `composer require drupal/noty_messages:8.x-1.x`.
 1. For core 8.7.x you will need also [this patch](https://www.drupal.org/project/drupal/issues/2987208#comment-13183097) 
    since 'status-messages' are a render element and we tap on top of it. This is fixed for 8.8.x. If you don't know how to apply patches check [this post](https://groups.drupal.org/node/518975).
 1. Enable the module `drush en noty_messages`.
 1. Clear the cache `drush cr`.
 1. Go to `/admin/reports/status` and check that the library is listed as installed there and you are done.

## Maintainers
 * Current
   - d70rr3s (https://www.drupal.org/user/318348)

 * Previous
   - MrMaksimize (http://drupal.org/user/801596)
   - iler (http://drupal.org/user/726092)

## Disclaimer
Currently the module is being ported to Drupal 8 and the integration is being refactored since use an outdated version of the Noty library. Design and API changes may occur until an 8.1 RC is out. You are advised not to use this module for other than testing purposes.
